import {Component, OnInit} from '@angular/core';
import Data from '../assets/data.json';
import * as AOS from 'aos';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  menuData: any;
  sectionData: any;
  faqData: any;
  galleryData: any;
  contactData: any;
  videoData: any;
  headerData: any;

  ngOnInit() {
    this.menuData = Data.menuData;
    this.headerData = Data.headerData;
    this.galleryData = Data.galleryData;
    this.videoData = Data.videoData;
    this.sectionData = Data.sectionData;
    this.faqData = Data.faqData;
    this.contactData = Data.contactData;
    AOS.init();
  }

}
