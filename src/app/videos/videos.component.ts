import { Component, Input, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-videos',
  templateUrl: './videos.component.html',
  styleUrls: ['./videos.component.css']
})
export class VideosComponent implements OnInit{

  @Input() data: string;
  safeURL: any;

  constructor(private _sanitizer: DomSanitizer) {}

  ngOnInit() {
    this.safeURL = this._sanitizer.bypassSecurityTrustResourceUrl(this.data['videoUrl']);
  }

}
